/*
	First, we load the expressjs module into our application and saved it in a variable called express
*/
const express = require('express')
/*
	Create an application with expressjs
	This creates an application that uses express and stores in a variable called app. Which makes it easier to use expressjs methods in our api
*/
const app = express()
/*
	port is just a variable to contain the port number we want to designate for our new expressjs api
*/
const port = 4000

/*
	To create a new route in expressjs, we first access from our express() package, our method. For get method request, access express(app), and use get()

	syntax:
		app.routeMethod('/',(req,res)=>{

			//function to handle request and response
		
		})
*/

// setup for allowing the server to handle data from requests
// allows your app to read json data
app.use(express.json())

app.get('/',(req,res)=>{

	// res.send() ends the response and sends your response to the client
	// res.send() already does the res.writeHead() and res.end() automatically
	res.send('Hello from our New ExpressJS API')
})

// Mini activity
app.get('/hello',(req,res)=>{
	res.send('Hello Batch 157!')
})

// app.post('/hello',(req,res)=>{
// 	console.log(req.body)
// 	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
// })

// Mini activity 2
app.post('/hello',(req,res)=>{
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}, I live in ${req.body.address}`)
})

let users=[]

app.post('/signup',(req,res)=>{
	console.log(req.body)
	if(req.body.username != '' && req.body.password != ''){
		users.push(req.body)
		
		res.send(`User ${req.body.username} successfully registered`)
		console.log(users)
	} else {
		res.send('Please input BOTH username and password')
	}
})

app.put('/change-password', (req,res)=>{
	let message;
	for(let i = 0;i<users.length;i++){
		if (req.body.username===users[i].username) {
			users[i].password=req.body.password
			message=`User ${req.body.username}'s password has been updated`
			break
		}else{
			message='User does not exist'
		}
	}
	res.send(message)
})

app.get('/home',(req,res)=>{
	res.send('Hello, welcome to the Home Page')
})

app.get('/users',(req,res)=>{
	res.send(users)
})

app.delete('/delete-user',(req,res)=>{
	let message
	for(let i = 0;i<users.length;i++){
		if(req.body.username===users[i].username) {
			users.splice(i,1)
			message=`User ${req.body.username} has been removed`
			break
		} else {
			message=`User does not exist`
		}
	}
	res.send(message)
})
/*
	app.listen() allows us to designate the correct port to our new expressjs api and once the server is running we can then run a function that runs a console.log() which contains a message that the server is running.
*/

app.listen(port,()=>{console.log(`Server is running at port ${port}`)})